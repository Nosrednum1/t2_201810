package model.data_structures;

import java.awt.Point;

public class Item<E extends Comparable<E>> {

	private String trip_id;

	private String taxi_id;

	private Point dropoff_centroid_location;

	private String company;

	public String getTaxi(){
		return taxi_id;
	}

	public String getTrip(){
		return trip_id;
	}

	public String getCompany(){
		return company;
	}

	public boolean isTaxi(){
		return (trip_id != "" && trip_id != null);
	}

	public int getLocation(){
		return 1;
	}

}
