package model.data_structures;

public class Node <E extends Comparable<E>>{

	private Node next;

	private E myInfo;

	private Node previous;

	public Node getNext(){return this.next;}

	public E getItem(){return this.myInfo;}

	public Node GetPrevious(){return this.previous;}

	public Node next(){return this.next;}

	public int size() {
		return (next != null)? 1+ next.size(): 1;
	}

	public boolean hasNext() {
		return (next != null);
	}

	public boolean hasPrev() {
		return (previous != null);
	}

	public Node<E> get(int position) {
		return (position == 1)?this:(next != null)? next.get(position-1):null;
	}

	public Node<E> get(E element) {
		return (myInfo.compareTo(element)==0)?this:(next != null)? next.get(element):null;
	}

	public boolean delete(E element) {
		boolean deleted = false;
		if(next != null && !deleted){
			if(((Item)next.getItem()).equals(element)){
				if(next.hasNext()){
					next.next().setPrev(this);
					setNext(next.next());

				}else{
					next.setPrev(null);
					setNext(null);;
					deleted = true;
				}
			}
			else{
				deleted = next.delete(element);
			}
		}
		return deleted;
	}

	public void setNext(Node<E> next){
		this.next = next;
	}
	public void setPrev(Node<E> prev){
		this.previous = prev;
	}
	public void setItem(E element){
		this.myInfo = element;
	}

	public boolean contais(E element) {
		boolean a = false;
		if(this.myInfo.compareTo(element) == 0){
			a = true;
		}
		else{
			if(next != null){
				a = contais(element);
			}
		}
		return a;
	}
}
