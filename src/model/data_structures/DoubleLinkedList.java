package model.data_structures;

public class DoubleLinkedList <E extends Comparable<E>> implements LinkedList<E>{

	private Node<E> root;

	private Node<E> current;

	public DoubleLinkedList() {
		root = null;
		current = root;
	}

	@Override
	public boolean hasNext(){
		return (current != null)?current.hasNext():false;
	}

	@Override
	public Node<E> next() {
		return (current.hasNext())? (current = current.next()) : null;
	}

	@Override
	public Node<E> getCurrent() {
		return this.current;
	}

	@Override
	public void listing() {
	}

	@Override
	public int size() {
		return (root != null )? root.size() : 0;
	}

	@Override
	public Node<E> get(int position) {
		return (root == null)? null: root.get(position);
	}

	@Override
	public Node<E> get(E Element) {
		return (root != null)? root.get(Element): null;
	}

	@Override
	public boolean delete(E element) {
		boolean deleted = false;
		if(root != null){
			if(!root.hasNext() && root.getItem().compareTo(element) == 0){
				root = null;deleted = true;}
			else {
				while(current.hasNext() && !deleted){
					current = current.next();
					if(current.next()!= null && current.next().getItem().compareTo(element) == 0){
						current.setNext(current.next().next());
						if(current.next().next() != null)
							current.next().next().setPrev(current);
						deleted = true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean add(E Element) {
		boolean r = false;
		Node theNew = new Node<>();
		theNew.setItem(Element);
		if(root == null){
			root = theNew;current = root; r=true;
		}
		else {
			while(current.hasNext()){
				current = current.next();
			}
			current.setNext(theNew);
			theNew.setPrev(current);
			r = true;
		}
		return r;
	}
	public boolean contains(E element){
		return (root!= null)? root.contais(element) : false;
	}
}
