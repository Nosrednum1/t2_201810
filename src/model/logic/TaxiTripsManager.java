package model.logic;

import java.io.*;

import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Item;
import model.data_structures.LinkedList;
import model.data_structures.Node;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	public Service[] services;

	public DoubleLinkedList<Service> servicesList = new DoubleLinkedList<>();

	public void loadServices (String serviceFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().create();
			services = gson.fromJson(reader, Service[].class);
			this.services = services;
			//			System.out.println("Object mode: " + services[5].getTripId());
			//			System.out.println("Object mode: " + services[6].getTripId());
			//			System.out.println("Object mode: " + services[7].getTripId());
			for (Service srvc : services) {
				servicesList.add(srvc);
				//				System.out.println("%s" + "Object mode: " + srvc.getTripId() );
				//				System.out.println("___\n");
			}
		} catch (FileNotFoundException ex) {
			System.out.println("The file doesn't exist ");
		}
		System.out.println("Inside loadServices with " + serviceFile);		
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		System.out.println("Inside getTaxisOfCompany with " + company);
		DoubleLinkedList<Taxi> taxisOf = new DoubleLinkedList<>();
		for (Service service : services) {
			if(service.getCompany() != null && service.getCompany() != ""){
				Taxi t = new Taxi(service.getTaxiId(), service.getCompany());
				if((service.getCompany().equals(company) ||
						service.getCompany().startsWith(company))){
					taxisOf.add(t);
				}
			}
		}
		//		while (servicesList.next() != null){
		//			if(servicesList.getCurrent().getItem().getCompany()!= null
		//					&& servicesList.getCurrent().getItem().getCompany() != ""
		//					&&servicesList.getCurrent().getItem().getCompany().startsWith(company)){
		//				Node a = servicesList.getCurrent();
		//				taxisOf.add(new Taxi(((Taxi)a.getItem()).getTaxiId(),((Taxi) a.getItem()).getCompany() ));
		//			}
		//		}
		return taxisOf;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		DoubleLinkedList<Service> servicesOf = new DoubleLinkedList<>();
		for (Service service : services) {
			if(service.getLocation() != 0){
				if(service.getLocation() == communityArea){
					servicesOf.add(service);
				}
			}
		}
		return servicesOf;
	}

	@Override
	public DoubleLinkedList<Service> getServices() {
		return servicesList;
	}


}
