package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;

	private String company;

	public Taxi(String id, String comp){
		this.company = comp;
		this.taxi_id = id;
	}

	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		return company;
	}

	@Override
	public int compareTo(Taxi o) {
		return (this.getTaxiId().equals(o.getTaxiId()))? 0 :
			(this.getTaxiId().compareTo(o.getTaxiId())> 0)? 1: -1;
	}	

	@Override
	public String toString() {
		return "   " + getTaxiId() + " == " + getCompany();
	}
}
