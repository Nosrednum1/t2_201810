package model.vo;

import java.awt.Point;
import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	private String trip_id;

	private String taxi_id;

	private String pickup_census_tract;

	private String dropoff_census_tract;

	private String payment_type;

	private String company;
	//	//	______________________
	//
	//	private Date trip_start_timestamp;
	//
	//	private Date trip_end_timestamp;
	//	//______________________

	private Point pickup_centroid_location;

	private Point dropoff_centroid_location;
	//	//______________________

	private double dropoff_centroid_latitude;

	private String dropoff_centroid_longitude;

	private int dropoff_community_area;

	private String extras;

	private String fare;

	private String pickup_centroid_latitude;

	private String pickup_centroid_longitude;

	private String pickup_community_area;

	private String tips;

	private String tolls;

	private double trip_miles;

	private double trip_total;

	private int trip_seconds;

	public int getLocation(){
		return dropoff_community_area;
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return trip_id;
	}	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxi_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return trip_miles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return trip_total;
	}

	@Override
	public int compareTo(Service o) {
		return (this.getTripId().equals(o.getTripId()))? 0 :
			(this.getTripId().compareTo(o.getTripId())> 0)? 1: -1;
	}

	@Override
	public String toString() {
		return "   " + getTripId() + " == " + company;
	}

	public String getCompany() {
		return company;
	}
}
